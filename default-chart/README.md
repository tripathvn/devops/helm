# default-chart

## Must override

### By `--values values.yaml`

```yaml
replicaCount: 1

image:
  repository: tripathvn/javabackendv2
  tag: "master@sha256:7670ec2c60a6fa5cb92345cdba6b9244f720ca881ba08208305cd43a294adef2"

hostNetwork: true
dnsPolicy: ClusterFirstWithHostNet

probe:
  path: /
  options:
    initialDelaySeconds: 20


nameOverride: "name"

service:
  port: 8989
  udp: true

ingress:
  host: haha.com

resources:
  requests:
    cpu: 269m
    memory: 169Mi
  limits:
    memory: 269Mi

gitlab:
  app: gitlabapp
  env: gitlabenv
```

## By `.gitlab-ci.yml`

```shell
helm repo add tripath https://gitlab.com/api/v4/projects/28991957/packages/helm/stable
helm install "$CI_ENVIRONMENT_SLUG" --set image.repository="$CI_REGISTRY_IMAGE" --set image.tag="$CI_COMMIT_REF_SLUG@$(cat $CI_PROJECT_DIR/digest.txt)" --set gitlab.app="$CI_PROJECT_PATH_SLUG" --set gitlab.env="$CI_ENVIRONMENT_SLUG" --set ingress.host="$DOMAIN" --set nameOverride="$CI_PROJECT_PATH_SLUG" tripath/default-chart 
```

